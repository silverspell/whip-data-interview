package tests;

import static org.junit.Assert.fail;

import java.util.Random;

import org.junit.Test;

import com.fusion.Backoff;



public class TestBackoff {

	@Test
	public void shouldReturnPositiveNumber() {
		Random random = new Random();
		int result = Backoff.getDelay(random.nextInt(), random.nextInt(100));
		if (result <= 0) {
			fail("Negative number " + result);
		}
	}
		

}
