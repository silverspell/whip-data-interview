package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fusion.RequestData;
import com.fusion.RequestTask;
import com.fusion.ServiceSettings;

import okhttp3.OkHttpClient;

public class TestRequestTask {

	private Semaphore semaphore;
	private OkHttpClient httpClient;
	private ServiceSettings serviceSettings;
	
	private ExecutorService executorservice = Executors.newSingleThreadExecutor(); 
	
	@Before
	public void setUp() throws Exception {
		serviceSettings = new ServiceSettings("http://localhost:8080/api/v1/greeting", 45, 2, 10, "GreetingTest");
		semaphore = new Semaphore(serviceSettings.getCapacity());
		httpClient = new OkHttpClient().newBuilder().build(); //Session store left out. We are bypassing service throttling.
		System.out.println("Creating");
	}

	@After
	public void tearDown() throws Exception {
		executorservice.shutdown();
	}

	@Test
	public void testRequestData() throws Exception {
		Future<Integer> future = executorservice.submit(new RequestTask(new RequestData(1, "test", serviceSettings.getName()), semaphore, serviceSettings, httpClient));
		System.out.println("Waiting for initial delay to pass.");
		assertEquals(new Integer(200), future.get());
	}
	
	@Test 
	public void testRequestsMoreThanCapacity() throws Exception {
		int drained = semaphore.drainPermits();
		Future<Integer> future = executorservice.submit(new RequestTask(new RequestData(1, "test ", serviceSettings.getName()), semaphore, serviceSettings, httpClient));
		
		try {
			future.get(5, TimeUnit.SECONDS); //wait 5 seconds
		} catch(TimeoutException tex) {
			return;
		} catch (Exception ex) {
			fail(ex.getMessage());
		} finally {
			semaphore.release(drained);
		}
		
	}
	
	@Test
	public void drainAndRelease() throws Exception {
		int drained = semaphore.drainPermits();
		List<Future<Integer>> futures = new ArrayList<>();
		for (int i = 0; i < 3; ++i) {
			Future<Integer> future = executorservice.submit(new RequestTask(new RequestData(1, "test ", serviceSettings.getName()), semaphore, serviceSettings, httpClient));
			futures.add(future);
		}
		
		int doneBeforeRelease = (int) futures.stream().filter(f -> f.isDone()).count();

		semaphore.release();
		Thread.sleep(1500); // sleep 1.5 second to allow at least 1 Task to complete
		int doneAfterRelease = (int) futures.stream().filter(f -> f.isDone()).count();

		if (doneAfterRelease - doneBeforeRelease != 1) {
			fail("Total completed: " + (doneAfterRelease - doneBeforeRelease));
		}
		
		semaphore.release(drained - 1);
	}
	
	@Test
	public void processInvalidUrl() {
		serviceSettings.setUrl("http://1/abc");
		RequestTask task = new RequestTask(new RequestData(1, "test ", serviceSettings.getName()), semaphore, serviceSettings, httpClient);
		int result = task.call();
		if (result != 404) {
			fail("Invalid url test failed");
		}
	}

	
	
}
