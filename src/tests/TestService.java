package tests;

import static org.junit.Assert.fail;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.junit.Before;
import org.junit.Test;

import com.fusion.RequestData;
import com.fusion.Service;
import com.fusion.ServiceSettings;

public class TestService {

	private ServiceSettings serviceSetting;
	private BlockingQueue<RequestData> requestQueue;
	private final int QUEUE_COUNT = 20 ; //arbitrary
	
	@Before
	public void setUp() throws Exception {
		serviceSetting = new ServiceSettings("http://localhost:8080/api/v1/greeting", 45, 2, 10, "GreetingTest");
		requestQueue = new ArrayBlockingQueue<>(QUEUE_COUNT);
		for (int i = 0; i < QUEUE_COUNT; ++i) {
			requestQueue.offer(new RequestData(i, i + "", serviceSetting.getName()));
		}
		
	}
	

	@Test
	public void testAllRequestsDone() throws Exception {
		Service service = new Service(serviceSetting, requestQueue);
		int count = service.call();
		if (count != QUEUE_COUNT) {
			fail("Item count not equal to the queue count");
		}
	}

}
