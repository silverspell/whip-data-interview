package com.fusion;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

public class CookieStore implements CookieJar {

	private Set<Cookie> store = new HashSet<>();
	
	@Override
	public List<Cookie> loadForRequest(HttpUrl arg0) {
		store.removeIf(cookie -> cookie.expiresAt() <= System.currentTimeMillis());
		return new ArrayList<Cookie>(store);
	}

	@Override
	public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
		store.addAll(cookies);		
	}

}
