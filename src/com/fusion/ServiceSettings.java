package com.fusion;

public class ServiceSettings {

	private String url;
	private int restoreRate;
	private int restoreBy;
	private int capacity;
	private String name;
	
	public ServiceSettings(String url, int restoreRate, int restoreBy, int capacity, String name) {
		this.url = url;
		this.restoreRate = restoreRate;
		this.restoreBy = restoreBy;
		this.capacity = capacity;
		this.name = name;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getRestoreRate() {
		return restoreRate;
	}
	public void setRestoreRate(int restoreRate) {
		this.restoreRate = restoreRate;
	}
	public int getRestoreBy() {
		return restoreBy;
	}
	public void setRestoreBy(int restoreBy) {
		this.restoreBy = restoreBy;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
}
