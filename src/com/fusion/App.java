package com.fusion;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

	private static Logger logger = LoggerFactory.getLogger(App.class);
	private static final int REQUEST_COUNT = 500;
	
	
	public static void main(String[] args) throws Exception {
		logger.info("Starting up...");
		long startTime = System.currentTimeMillis();
		List<ServiceSettings> settings = JsonUtils.readServiceSettings();
		List<Future<Integer>> futures = new ArrayList<>();
		List<ExecutorService> serviceExecutors = new ArrayList<>();
		
		
		final Consumer<ServiceSettings> poolConsumer = (serviceSetting) -> {
			ArrayBlockingQueue<RequestData> queue = new ArrayBlockingQueue<>(REQUEST_COUNT);
			ExecutorService executorService = Executors.newCachedThreadPool();
			for (int i = 0; i < REQUEST_COUNT; ++i) {
				queue.offer(new RequestData(i, i + "", serviceSetting.getName()));
			}
			futures.add(executorService.submit(new Service(serviceSetting, queue)));
			serviceExecutors.add(executorService);			
		};
		
		settings.forEach((setting) -> poolConsumer.accept(setting));
		
		int total = 0;
		
		for (Future<Integer> future: futures) {
			total += future.get();
		}
		long endTime = System.currentTimeMillis();
		
		logger.info("Completed " + total + " requests in " + (endTime - startTime) / 1000 + " seconds.");
		for (ExecutorService es: serviceExecutors) {
			es.shutdown();
			es.shutdownNow();
		}
		
	}
}
