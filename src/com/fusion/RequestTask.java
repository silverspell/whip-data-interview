package com.fusion;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RequestTask implements Callable<Integer> {

	private static final Logger logger = LoggerFactory.getLogger(RequestTask.class);

	private final RequestData requestData;
	private final Semaphore currentCapacity;
	private final ServiceSettings serviceSettings;
	private final OkHttpClient httpClient;

	private final ExecutorService executorService;

	private final int HTTP_OK = 200;
	private final int HTTP_TOO_MANY_REQUESTS = 429;
	private final int HTTP_ERROR = 500;
	private final int HTTP_NOT_FOUND = 404;

	private class Task implements Callable<Response> {

		final int delay;

		public Task(final int delay) {
			this.delay = delay;
		}

		@Override
		public Response call() {
			Response response = null;
			try {
				Thread.sleep(delay * 1000);
				logger.info("Making http request... " + requestData);
				Request request = new Request.Builder().url(serviceSettings.getUrl()).build();
				

				response = httpClient.newCall(request).execute();
			} catch (Exception e) {
				logger.error("Network is probably unreachable.");
				response = new Response.Builder().code(HTTP_NOT_FOUND).build();
			}
			return response;
		}

	};

	/**
	 * 
	 * @param requestData
	 * 			@see {@link RequestData}
	 * @param currentCapacity
	 *            Capacity limiter semaphore.
	 * @param serviceSettings
	 * 			@see {@link ServiceSettings}
	 * @param httpClient
	 *            OkHttpClient.
	 */
	public RequestTask(RequestData requestData, Semaphore currentCapacity, ServiceSettings serviceSettings,
			OkHttpClient httpClient) {

		this.requestData = requestData;
		this.currentCapacity = currentCapacity;
		this.serviceSettings = serviceSettings;
		this.httpClient = httpClient;

		// executorService = Executors.newScheduledThreadPool(1);
		executorService = Executors.newFixedThreadPool(1);

	}

	@Override
	public Integer call() {
		int attempt = 1;
		int delay = 1;// Backoff.getDelay(attempt,
						// serviceSettings.getRestoreRate()); //initial delay
		int result = 0;
		int totalDelay = delay;

		while (true) {

			try {
				if (!currentCapacity.tryAcquire(delay, TimeUnit.MILLISECONDS)) {
					continue;
				}

				Future<Response> future = executorService.submit(new Task(delay));

				Response response = null;
				try {
					response = future.get();
					result = response.code();
					response.body().close();
				} catch (Exception ex) {
					result = HTTP_NOT_FOUND;
				}
				
				
				if (result == HTTP_TOO_MANY_REQUESTS || result >= HTTP_ERROR) {
					attempt++;
					delay = Backoff.getDelay(attempt, serviceSettings.getRestoreRate());
					totalDelay += delay;
					logger.debug("Got " + result + "... Will sleep for " + delay + " seconds... " + requestData
							+ ", delay: " + delay + ", total delay: " + totalDelay + ", attempt: " + attempt);
					Thread.sleep(delay * 1000);

				} else if (result >= HTTP_NOT_FOUND && result < HTTP_ERROR) {
					logger.error("Invalid url! " + " Got " + result + " from " + serviceSettings.getUrl());
					break;
				} else if (result == HTTP_OK) { // 200
					logger.debug("Successful request.... " + requestData + ", total delay: " + totalDelay
							+ ", attempt: " + attempt);
					break;
				}

			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
			}

		}

		executorService.shutdown();
		currentCapacity.release();
		return result;
	}

}
