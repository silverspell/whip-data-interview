package com.fusion;

import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

public class JsonUtils {

	public static List<ServiceSettings> readServiceSettings() throws Exception {
		Gson gson = new GsonBuilder().create();
		JsonReader reader = new JsonReader(new FileReader("services.json"));
		ServiceSettings[] serviceSettings = gson.fromJson(reader, ServiceSettings[].class);
		return Arrays.asList(serviceSettings);
	}
	
}
