package com.fusion;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import okhttp3.OkHttpClient;

public class Service implements Callable<Integer> {

	private static final Logger logger = LoggerFactory.getLogger(Service.class);

	private final ServiceSettings serviceSettings;
	private final BlockingQueue<RequestData> requestQueue;

	private final ExecutorService queueProcessor;
	private final ScheduledExecutorService timerProcessor;

	private final List<Future<Integer>> futures;

	private final OkHttpClient httpClient;
	
	private final Semaphore capacityProvider;
	
	/**
	 * Service class. Creates 2 main thread pools and controls the execution.
	 * @param serviceSettings @see {@link ServiceSettings}
	 * @param requestQueue Blocking queue passed from producer.
	 */
	public Service(ServiceSettings serviceSettings, BlockingQueue<RequestData> requestQueue) {
		this.serviceSettings = serviceSettings;
		this.requestQueue = requestQueue;
		
		this.queueProcessor = Executors.newFixedThreadPool(serviceSettings.getCapacity());
		this.timerProcessor = Executors.newScheduledThreadPool(1);

		this.httpClient = new OkHttpClient().newBuilder().cookieJar(new CookieStore()).build();
		this.futures = new ArrayList<>(); //results.
		
		capacityProvider = new Semaphore(serviceSettings.getCapacity()); 
		
		//A timer component that counts the completed count. Not necessary for the assignment. 
		timerProcessor.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				long count = futures.stream().filter(f -> f.isDone()).count();
				logger.debug(serviceSettings.getName() +  " completed count: " + count);
			}

		}, serviceSettings.getRestoreRate(), serviceSettings.getRestoreRate(), TimeUnit.SECONDS);

	}

	@Override
	public Integer call() throws Exception {
		logger.debug(serviceSettings.getName() + " rolling on... ");

		while (!requestQueue.isEmpty()) {
			RequestData requestData = requestQueue.take(); // this should normally block but in this scenario all items are passed to the queueProcessor queue. 
			futures.add(queueProcessor
					.submit(new RequestTask(requestData, capacityProvider, serviceSettings, httpClient)));
		}
		
		for (Future<Integer> f: futures) { //This block will block until all futures are completed.
			f.get();
		}
		
		// clean up
		timerProcessor.shutdown();
		queueProcessor.shutdown();
		logger.debug("Service " + serviceSettings.getName() + " finished with " + futures.size() + " requests.");
		return futures.size();
	}

}
