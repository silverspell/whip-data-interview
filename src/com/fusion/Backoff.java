package com.fusion;

import java.util.Random;

public class Backoff {

	private static Random random = new Random();

	
	/**
	 * Delay function. 
	 * @param attemptCount attempt count.
	 * @param restoreRate @see {@link ServiceSettings}
	 * @return delay in seconds.
	 */
	public static int getDelay(int attemptCount, int restoreRate) {
		if (restoreRate == 0) {
			restoreRate = 1;
		}
		return (int) Math.abs(Math.ceil(((attemptCount % 4) + 1) * restoreRate * random.nextDouble()));
	}

}
