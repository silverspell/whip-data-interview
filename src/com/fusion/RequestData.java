package com.fusion;

public class RequestData {

	private long id;
	private String name;
	private String serviceName;
	
	
	
	public RequestData(long id, String name, String serviceName) {
		super();
		this.id = id;
		this.name = name;
		this.serviceName = serviceName;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@Override
	public String toString() {
		return "RequestData [id=" + id + ", name=" + name + ", serviceName=" + serviceName + "]";
	}
	
	
	
}
